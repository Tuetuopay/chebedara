var express = require('express');
var router = express.Router();

var joi = require ('joi'), mysql = require ('mysql');

global.pool = mysql.createPool ({
	connectionLimit: 10,
	host           : 'mysql',
	user           : 'chebedara',
	password       : 'vivalarevolucion',
	database       : 'chebedara',
	multipleStatements: true
});

function makeSessionMessage (title, body, type) {
	return {
		type: type || "danger",
		title: title,
		message: body
	};
}
const stdSqlMessage = makeSessionMessage ('Oups !', "Le serveur SQL fait la révolution. Aidez-le et réessayez plus tard.", "danger");

function chunkify(a, n, balanced) {
	if (n < 2)
		return [a];

	var len = a.length, out = [], i = 0, size;

	if (len % n === 0) {
		size = Math.floor(len / n);
		while (i < len)
			out.push(a.slice(i, i += size));
	}
	else if (balanced) {
		while (i < len) {
			size = Math.ceil((len - i) / n--);
			out.push(a.slice(i, i += size));
		}
	}
	else {
		n--;
		size = Math.floor(len / n);
		if (len % size === 0)
			size--;
		while (i < size * n)
			out.push(a.slice(i, i += size));
		out.push(a.slice(size * n))
	}

	return out;
}

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render ('index', {session: req.session});
})
.post ('/register', function (req, res) {
	joi.validate (req.body, {
		name: joi.string ().min (4).required (),
		email: joi.string ().regex (/^[a-zA-Z0-9._%+-]+@(telecom-paristech|enst)\.fr$/).required (),
		password: joi.string ().min (8).required ()
	}, function (joiErr) {
		if (joiErr) {
			res.end (JSON.stringify ({success: false, error: joiErr.details[0].path}));
			return;
		}

		// Check if the email is already in use
		pool.query('SELECT COUNT(id) AS cnt FROM users WHERE email = ?', req.body.email, function (err, rows) {
			if (err) {
				res.end (JSON.stringify ({success: false, error: 'sql'}));
				console.trace (err);
				return;
			}
			if (rows[0].cnt != 0) {
				res.end (JSON.stringify ({success: false, error: 'email in use'}));
				return;
			}

			// Registering the user
			var pass = protect (req.body.email, req.body.password);
			pool.query('INSERT INTO users(`name`, location, email, `password`, breakfastComment) VALUES(?, "", ?, ?, "")',
				[req.body.name, req.body.email, pass], function (err, rows) {
					if (err) {
						res.end (JSON.stringify ({success: false, error: 'sql'}));
						console.trace (err);
						return;
					}
					res.end (JSON.stringify ({success: true}));
				})
		});
	});
})
.post ('/login', function (req, res) {
	// 	Get encrypted PW
	var password = protect (req.body.email, req.body.password);
	pool.query('SELECT * FROM users WHERE email = ? AND password = ?', [req.body.email, password], function (err, rows) {
		if (err) {
			req.session.message = stdSqlMessage;
			console.trace (err);
		}
		else if (rows.length == 0)
			req.session.message = makeSessionMessage ('Connexion échouée.', "L'email ou le mot de passe sont incorrects.", "danger");
		else {
			rows = rows[0];
			req.session = {
				message: makeSessionMessage ('Weeeeeeee !', "Vous êtes maintenant connectés.", "info"),
				id: rows.id,
				admin: rows.admin,
				name: rows.name,
				dinner: rows.repas,
				location: rows.location
			};
		}
		res.redirect ('/');
	});
})
.get ('/logout', function (req, res) {
	req.session = null;
	res.redirect ('/');
})
.get ('/petits-dej', function (req, res, next) {
	if (req.session.id) {
		// Fetching the users comment AND menu AND the current user order
	pool.query('SELECT breakfastComment, breakfastTime FROM users WHERE id = ?; \
					SELECT * FROM breakfast_items; \
					SELECT breakfast_order.*, breakfast_items.name FROM breakfast_order \
					LEFT JOIN breakfast_items ON breakfast_order.item = breakfast_items.id \
					WHERE user = ?;' + (req.session.admin ?
				'SELECT breakfast_order.*, breakfast_items.name, \
					users.id AS uid, users.name AS uname, users.location AS uloc, users.breakfastComment AS ucomment, \
					users.breakfastTime AS utime \
				 FROM breakfast_order \
				 LEFT JOIN breakfast_items ON breakfast_order.item = breakfast_items.id\
				 LEFT JOIN users ON users.id = breakfast_order.user' : ''),
			[req.session.id, req.session.id], function (err, rows) {
				if (err) {
					req.session.message = stdSqlMessage;
					res.redirect ('/');
					console.trace (err);
				}
				else {
					var allOrders = {};
					if (req.session.admin) {
						rows[3].forEach (function (r) {
							if (allOrders[r.uid])
								allOrders[r.uid].push (r);
							else
								allOrders[r.uid] = [r];
						});
						for (var uid in allOrders)
							if (allOrders.hasOwnProperty (uid)) {
								var info = allOrders[uid][0];
								allOrders[uid] = { desc: allOrders[uid].map (function (i) { return i.name + " x" + i.count; }).join (', ') };
								for (var i in info) if (info.hasOwnProperty (i)) allOrders[uid][i] = info[i];
							}
					}
					res.render ('breakfasts', {session: req.session, menu: rows[1], order: rows[2], allOrders: allOrders, comment: rows[0][0].breakfastComment, time: rows[0][0].breakfastTime});
				}
			});
	}
	else {
		req.session.message = makeSessionMessage ('STOP !', "Vous devez être connectés pour prendre un petit-déjeuner !");
		res.redirect ('/');
	}
})
.get ('/diner', function (req, res, next) {
	if (req.session.id) {
		if (req.session.admin) {
			pool.query('SELECT `name` FROM users WHERE repas = TRUE ORDER BY `name`', function (err, rows) {
				if (err) {
					req.session.message = stdSqlMessage;
					res.redirect ('/');
					console.trace (err);
				}
				else {
					var users = chunkify (rows, 4, true);
					res.render ('dinner', {session: req.session, users: users});
				}
			});
		}
		else res.render ('dinner', {session: req.session});
	}
	else  {
		req.session.message = makeSessionMessage ('STOP !', "Vous devez être connectés pour vous inscrire au repas de promo !");
		res.redirect ('/');
	}
})
.post ('/petits-dej/cart/add/:id/:count', function (req, res, next) {
	if (req.session.id) {
		if (req.params.id > 0 && req.params.count > 0) {
			if (req.params.count > 5) {
				res.respond (false, 'amount');
				return;
			}
			// Checking the given item id exists
			pool.query("SELECT COUNT(id) AS cnt FROM breakfast_items WHERE id = ?", req.params.id, function (err, rows) {
				if (err) {
					res.respond (false, 'sql');
					console.trace (err);
				}
				else if (rows[0].cnt == 0)
					res.respond (false, 404);
				else {
					// Check whether it exists in the user's cart
					pool.query('SELECT id FROM breakfast_order WHERE user = ? AND item = ?', [req.session.id, req.params.id], function (err, rows) {
						if (err) {
							res.respond (false, 'sql');
							console.trace (err);
							return;
						}
						var sql = "";
						if (rows.length == 0) sql = 'INSERT INTO breakfast_order(count, user, item) VALUES(?, ?, ?)';
						else sql = 'UPDATE breakfast_order SET count = ? WHERE user = ? AND item = ?';
						// Add it !
						pool.query (sql, [req.params.count, req.session.id, req.params.id], function (err, rows) {
							if (err) {
								res.respond (false, 'sql');
								console.trace (err);
							}
							else res.respond (true);
						});
					});
				}
			});
		}
	}
	else next ();
})
.delete ('/petits-dej/cart/delete/:id', function (req, res, next) {
	if (req.session.id) {
		// Deleting
		pool.query('DELETE FROM breakfast_order WHERE user = ? AND id = ?', [req.session.id, req.params.id], function (err, rows) {
			if (err) {
				res.respond (false, 'sql');
				console.trace (err);
			}
			else res.respond (true);
		})
	}
	else next ();
})
.put ('/petits-dej/cart/location', function (req, res, next) {
	if (req.session.id) {
		pool.query('UPDATE users SET location = ? WHERE id = ?', [req.body.location, req.session.id], function (err, row) {
			if (err) {
				res.respond (false, 'sql');
				console.trace (err);
			}
			else {
				req.session.location = req.body.location;
				res.respond (true);
			}
		});
	}
	else next ();
})
.put ('/petits-dej/cart/comment', function (req, res, next) {
	if (req.session.id) {
		pool.query('UPDATE users SET breakfastComment = ? WHERE id = ?', [req.body.comment, req.session.id], function (err, result) {
			if (err) {
				res.respond (false, 'sql');
				console.trace (err);
			}
			else {
				console.log (result);
				res.respond (true);
			}
		});
	}
	else next ();
})
.put ('/petits-dej/cart/time', function (req, res, next) {
	if (req.session.id) {
		joi.validate (req.body, {
			hour: joi.number ().min (0).max (12).required (),
			minute: joi.number ().min (0).max (59).required ()
		}, function (joiErr) {
			if (joiErr) {
				res.respond (false, joiErr.details[0].path);
				return;
			}

			pool.query('UPDATE users SET breakfastTime = ? WHERE id = ?', [req.body.hour + ":" + req.body.minute, req.session.id], function (err) {
				if (err) {
					if (err) {
						res.respond (false, 'sql');
						console.trace (err);
						return;
					}
					res.respond (true);
				}
			});
		});
	}
	else next ();
})

.put ('/petits-dej/menu/:id', function (req, res, next) {
	if (req.session.admin) {
		// Let's edit this !
		pool.query('UPDATE breakfast_items SET name = ? WHERE id = ?', [req.body.name, req.params.id], function (err, rows) {
			if (err) {
				res.respond (false, 'sql');
				console.trace (err);
			}
			else res.respond (true);
		});
	}
	else next ();
})
.post ('/petits-dej/menu', function (req, res, next) {
	if (req.session.admin) {
		// Let's edit this !
		pool.query('INSERT INTO breakfast_items(name) VALUES(?)', req.body.name, function (err, rows) {
			if (err) {
				res.respond (false, 'sql');
				console.trace (err);
			}
			else res.respond (true);
		});
	}
	else next ();
});

module.exports = router;
