/*
 Theme: Flatfy Theme
 Author: Andrea Galanti
 Bootstrap Version
 Build: 1.0
 http://www.andreagalanti.it
 */

String.prototype.replaceMustache = function (dict) {
	var str = this, r = new RegExp ();
	for (var key in dict)
		if (dict.hasOwnProperty (key)) {
			r.compile ('{{' + key + '}}', 'g');
			str = str.replace (r, dict[key]);
		}
	return str;
};

jQuery(function($) {
	$(document).ready( function() {
		$('.navbar-default').stickUp();

	});
});

$ (document).ready (function () {
	//animated logo
	$(".navbar-brand").hover(function () {
		$(this).toggleClass("animated shake");
	});

	// Wow Animation DISABLE FOR ANIMATION MOBILE/TABLET
	wow = new WOW ({
		mobile: false
	});
	wow.init ();

	// SmoothScroll
	$ ('a[href*=#]').click (function () {
		if (location.pathname.replace (/^\//, '') == this.pathname.replace (/^\//, '')
			&& location.hostname == this.hostname) {
			var $target = $ (this.hash);
			$target     = $target.length && $target || $ ('[name=' + this.hash.slice (1) + ']');
			if ($target.length) {
				var targetOffset = $target.offset ().top;
				$ ('html,body').animate ({scrollTop: targetOffset}, 600);
				return false;
			}
		}
	});

	$("#programme-flipbook").turn ({
		height: 480,
		autoCenter: true
	});

	$('#register-btn').popover ({
		content: $('#tpl-popover-register')[0].innerHTML,
		html: true,
		placement: "auto",
		title: "Inscription",
		trigger: "click",
		container: "body > nav"
	});
	$('#login-btn').popover ({
		content: $('#tpl-popover-login')[0].innerHTML,
		html: true,
		placement: "auto",
		title: "Connexion",
		trigger: "click",
		container: "body > nav"
	});

	setTimeout (function () { $('#alert-container > *').remove (); }, 5000);
});

var alertsCount = 0;
function addAlert (contents, type) {
	$('#alert-container').append ('<div class="alert alert-{{type}}" id="alert-no-{{id}}">{{contents}}</div>'
		.replace ('{{type}}', type).replace ('{{contents}}', contents).replace ('{{id}}', ++alertsCount));
	setTimeout (function () {
		$('#alert-no-' + alertsCount).remove ();
	}, 3000);
}

function doRegister () {
	$('#reg-cont').find('div.col-md-12').removeClass ('has-error').find('.help-block').text ("");
	$.ajax ({
		type: "POST",
		url: "/register",
		data: {
			name: $('#reg-name')[0].value,
			email: $('#reg-email')[0].value,
			password: $('#reg-password')[0].value
		},
		success: function (res) {
			res = JSON.parse (res);
			if (res.success)
				addAlert ("<strong>Weeeeeeee !</strong> Votre compte a bien été enregistré, vous pouvez dès à présent vous connecter.", "info");
			else {
				switch (res.error) {
					case "email in use":
						$('#reg-email-cont').addClass ('has-error');
						$('#reg-email-help').text ('Cette adresse email est déjà utilisée');
						break;
					case "sql":
						addAlert ("<strong>Oups !</strong> Impossible d'enregistrer votre compte : le serveur SQL fait la révolution.<br>Réessayez plus tard.", "danger");
						break;
					default:
						$('#reg-' + res.error + '-cont').addClass ('has-error');
						$('#reg-' + res.error + '-help').text ({
							name: "Ce nom n'est pas valide (4 carac. min).",
							email: "Cette adresse email n'est pas valide.",
							password: "Ce mot de passe n'est pas valide (8 caractères min).",
							location: "Tu t'es cru où ? (4 carac. min)."
						}[res.error]);
				}
			}
		}
	});
}
