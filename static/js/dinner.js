var socket = io.connect('chebedara.com:443');

var counter = document.getElementById ('spare-count'), registered = document.getElementById ('is-registered').value == 1;
console.log (registered);

socket.on ('setCount', function (count) {
	counter.innerHTML = count;

	if (!registered) {
		if (count > 0) $('#btn-register-dinner').removeClass ('hidden');
		else $('#btn-register-dinner').addClass ('hidden');
	}
});

function registerToDinner () {
	socket.emit ('register');
}
function unregisterToDinner () {
	socket.emit ('unregister');
}

socket.on ('result', function (status) {
	switch (status) {
		case 'sql':
			addAlert ("<strong>Oups !</strong> Action échouée : le serveur SQL fait la révolution.<br>Réessayez plus tard.", "danger");
			break;
		case 'registered':
			addAlert ("<strong>Tut tut tut ...</strong> Vous êtes déjà inscrits pour le repas de promo !", "danger");
			break;
		case 'unregistered':
			addAlert ("<strong>Tut tut tut ...</strong> Vous êtes pas encore inscrits pour le repas de promo !", "danger");
			break;
		case 'zero':
			addAlert ("<strong>Dommange !</strong> Il n'y a plus de place pour le repas !", "danger");
			break;
		case "ok":
			addAlert ("<strong>C'est noté</strong>", "info");
			$('#btn-register-dinner').toggleClass ('hidden');
			$('#btn-unregister-dinner').toggleClass ('hidden');
			registered = !registered;
	}
});
