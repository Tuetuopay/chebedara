function genericHandler (res) {
	res = JSON.parse (res);
	if (res.success) location.reload();
	else addAlert ("<strong>Oups !</strong> Y'a eu une erreur (" + res.error + ").");
}

function setEditItem (id, name) {
	document.getElementById ('admin-edit-id').value = id;
	document.getElementById ('admin-edit-name').value = name;
}

function editItem () {
	var id = document.getElementById ('admin-edit-id').value, name = document.getElementById ('admin-edit-name').value;
	$.ajax ({
		type: 'PUT',
		url: '/petits-dej/menu/' + id,
		data: { name: name },
		success: genericHandler
	});
}

function addToMenu () {
	var name = document.getElementById ('admin-add-name').value;
	$.ajax ({
		type: 'POST',
		url: '/petits-dej/menu',
		data: { name: name },
		success: genericHandler
	});
}
