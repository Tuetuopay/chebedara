// Set of function for the breakfast thingy

function addToCart (itemId) {
	// Fetching the count
	var count = document.getElementById ('menu-count-' + itemId);
	if (!count) return;
	count = count.value;
	if (count > 0)
		$.ajax ({
			type: 'POST',
			url: '/petits-dej/cart/add/' + itemId + '/' + count,
			success: function (res) {
				res = JSON.parse (res);
				if (res.success) location.reload ();
				else {
					switch (res.error) {
						case 'sql':
							addAlert ("<strong>Oups !</strong> Impossible d'enregistrer le panier : le serveur SQL fait la révolution.<br>Réessayez plus tard.", "danger");
							break;
						case 404:
							addAlert ("<strong>Hum ...</strong> Cet objet n'existe pas.", "danger");
							break;
						case 'amount':
							addAlert ("<strong>Bien essayé !</strong> Mais vous ne pouvez commander que 5 à la fois.", "danger");
							break;
						default:
							addAlert ("<strong>Oups !</strong> Erreur inconnue.", "danger");
					}
				}
			}
		});
}
function removeFromCart (itemId) {
	$.ajax ({
		type: "DELETE",
		url: '/petits-dej/cart/delete/' + itemId,
		success: function (res) {
			res = JSON.parse (res);
			if (res.success) location.reload ();
			else {
				switch (res.error) {
					case 'sql':
						addAlert ("<strong>Oups !</strong> Impossible d'enregistrer le panier : le serveur SQL fait la révolution.<br>Réessayez plus tard.", "danger");
						break;
					default:
						addAlert ("<strong>Oups !</strong> Erreur inconnue.", "danger");
				}
			}
		}
	});
}

function setLocation () {
	var loc = document.getElementById ('text-loc').value;
	$.ajax ({
		type: 'PUT',
		url: '/petits-dej/cart/location',
		data: { location: loc },
		success: function (res) {
			res = JSON.parse (res);
			if (res.success) location.reload ();
			else {
				switch (res.error) {
					case 'sql':
						addAlert ("<strong>Oups !</strong> Impossible d'enregistrer l'adresse de livraison : le serveur SQL fait la révolution.<br>Réessayez plus tard.", "danger");
						break;
					default:
						addAlert ("<strong>Oups !</strong> Erreur inconnue.", "danger");
				}
			}
		}
	});
}

function setComment () {
	var comment = document.getElementById ('comments').value;
	$.ajax ({
		type: 'PUT',
		url: '/petits-dej/cart/comment',
		data: { comment: comment },
		success: function (res) {
			res = JSON.parse (res);
			if (res.success) location.reload ();
			else {
				switch (res.error) {
					case 'sql':
						addAlert ("<strong>Oups !</strong> Impossible d'enregistrer votre commentaire : le serveur SQL fait la révolution.<br>Réessayez plus tard.", "danger");
						break;
					default:
						addAlert ("<strong>Oups !</strong> Erreur inconnue.", "danger");
				}
			}
		}
	});
}

function setOrderTime () {
	$.ajax ({
		type: 'PUT',
		url: '/petits-dej/cart/time',
		data: {
			hour: document.getElementById ('order-hour').value,
			minute: document.getElementById ('order-minute').value
		},
		success: function (res) {
			res = JSON.parse (res);
			if (res.success) location.reload ();
			else {
				switch (res.error) {
					case 'sql':
						addAlert ("<strong>Oups !</strong> Impossible d'enregistrer votre commentaire : le serveur SQL fait la révolution.<br>Réessayez plus tard.", "danger");
						break;
					case 'hour':
						addAlert ("<strong>Ahem ...</strong> L'heure est invalide (entre minuit et midi).", "danger");
						break;
					case 'minute':
						addAlert ("<strong>Ahem ...</strong> Les minutes sont invalides (0 et 59 ...).", "danger");
						break;
					default:
						addAlert ("<strong>Oups !</strong> Erreur inconnue.", "danger");
				}
			}
		}
	});
}
