var express      = require ('express');
var path         = require ('path');
var favicon      = require ('serve-favicon');
var logger       = require ('morgan');
var cookieParser = require ('cookie-parser');
var bodyParser   = require ('body-parser');
var cookieSession = require('cookie-session')({secret: 'VhjwCpA4SjHLRHFmWldyoQH36o9Bf1IytvQsQooe'});
var crypto = require ("crypto");

var routes = require ('./routes/index');

var app = express ();

global.protect = function (user, pass)   {
	for (var i = 0; i < 1000; i++)
		pass = crypto.createHash("sha512").update (user).digest ("hex").toString () + crypto.createHash("sha512").update (pass + user).digest ("hex").toString ();
	return pass;
};
express.response.respond = function (success, data, extra) {
	if (success)
		this.end (JSON.stringify ({success: true, data: data}) + '\n');
	else if (extra)
		this.end (JSON.stringify ({success: false, error: data, extra: extra}) + '\n');
	else
		this.end (JSON.stringify ({success: false, error: data}) + '\n');
};

// view engine setup
app.set ('views', path.join (__dirname, 'views'));
app.set ('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use (logger ('dev'));
app.use (bodyParser.json ());
app.use (bodyParser.urlencoded ({extended: false}));
// app.use (cookieParser ());
if (app.get ('env') === 'development') {
	app.use ("/static", express.static (path.join (__dirname, 'static')));
}
app.use (function (req, res, next) {
	res.header ("X-Powered-By", "Electricity");
	next ();
});

app.use (cookieSession);

app.use ('/', routes);

// catch 404 and forward to error handler
app.use (function (req, res, next) {
	var err    = new Error ('Not Found');
	err.status = 404;
	next (err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get ('env') === 'development') {
	app.use (function (err, req, res, next) {
		res.status (err.status || 500);
		res.render ('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use (function (err, req, res, next) {
	res.status (err.status || 500);
	res.render ('error', {
		message: err.message,
		error: {}
	});
});

app.onload = function () {
	var io = require ('socket.io')(app.server);

	// Adapt the cookie-session express middleware to SocketIO
	io.use (function (socket, next) {
		cookieSession (socket.request, socket.request.res, next);
	});

	var availableDinnerSeats = 200;
	pool.query('SELECT COUNT(id) AS cnt FROM users WHERE repas = TRUE', function (err, rows) {
		if (err) {
			console.trace (err);
			availableDinnerSeats = 0;
			return;
		}

		availableDinnerSeats = 200 - rows[0].cnt;
		var sockets = io.listen (1338);
		function addSeats (amount) {
			availableDinnerSeats += amount;
			sockets.emit ('setCount', availableDinnerSeats);
		}

		sockets.on ('connection', function (socket) {
			var session = socket.request.session;

			if (session.id) {
				socket.emit ('setCount', availableDinnerSeats);
				socket.on ('register', function () {
					if (availableDinnerSeats > 0) {
						// Lock seat before async SQL
						addSeats (-1);

						pool.query ('UPDATE users SET repas = TRUE WHERE id = ?', session.id, function (err, res) {
							if (err || res.changedRows == 0) { // Error or user already registered
								// Release locked seat
								addSeats (1);
								if (err) {
									socket.emit ('result', 'sql');
									console.trace (err);
								}
								else socket.emit ('result', 'registered');
							}
							else {
								session.dinner = 1;
								socket.emit ('result', 'ok');
							}
						});
					}
					else socket.emit ('result', 'zero');
				});
				socket.on ('unregister', function () {
					if (availableDinnerSeats < 200) {
						pool.query('UPDATE users SET repas = FALSE WHERE id = ?', session.id, function (err, res) {
							if (err || res.changedRows == 0) { // Error or user already registered
								if (err) {
									socket.emit ('result', 'sql');
									console.trace (err);
								}
								else socket.emit ('result', 'unregistered');
							}
							else {
								addSeats (1);
								session.dinner = 0;
								socket.emit ('result', 'ok');
							}
						});
					}
				});
			}
		});
	});
};

module.exports = app;
